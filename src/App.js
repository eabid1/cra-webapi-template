import React from 'react';
import './App.css';
import { Routes, Route } from "react-router-dom";
import Home from './components/Home';
import FetchData from './components/FetchData';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="fetch-data" element={<FetchData />} />
      </Routes>
    </div>
  );
}

export default App;
