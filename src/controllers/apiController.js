const axios = require('axios');
export const api = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
	timeout: 10000,
	accept: '*/*',
});