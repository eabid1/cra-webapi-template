import { api } from '../apiController';

export const getChallenges = async () => {
	const response = await api.get( '/api/Challenges' );
	console.log( response );
	return response;
}