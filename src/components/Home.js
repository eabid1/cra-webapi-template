import React from 'react'
import { useNavigate } from 'react-router-dom'
import logo from '../logo.svg';

const Home = () => {
	const navigate = useNavigate();
	const fetchDataHandler = () => {
		navigate("/fetch-data");
	};
	return (
		<header className="App-header">
			<img src={logo} className="App-logo" alt="logo" />
			<p>
				Edit <code>src/App.js</code> and save to reload.
			</p>
			<button
				className="App-link"
				onClick={fetchDataHandler}
			>
				Fetch Data
			</button>
		</header>
	)
};

export default Home;

