import React, { useState, useEffect } from 'react';

import { getChallenges } from '../controllers/challengesController';

import './FetchData.css'

const FetchData = () => {
	const [challenges, setChallenges] = useState([]);
	const [loading, setLoading] = useState(true);
	
	
	useEffect(() => {
		getChallenges().then((response) => {
			setLoading(false);
			if (response) {
				setChallenges(response.data);
			}
		}
		
	);
	}, []);
	
	return (
		<div className='fetch-data-container'>
			<h1 id="tabelLabel" >Games for Good Challenges</h1>
			<p>This component demonstrates fetching data from the server.</p>
			{ loading ?
				<p><em>Loading...</em></p>
				:
				<table className='table table-striped' aria-labelledby="tabelLabel">
					<thead>
					<tr>
						<th>Name</th>
					</tr>
					</thead>
					<tbody>
					{challenges.map(challenge =>
						<tr key={challenge.id}>
							<td>{challenge.name}</td>
						</tr>
					)}
					</tbody>
				</table>
			}
		</div>
	);
}

export default FetchData;
